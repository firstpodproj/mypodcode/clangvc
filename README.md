# ClangVC

[![CI Status](https://img.shields.io/travis/TYF/ClangVC.svg?style=flat)](https://travis-ci.org/TYF/ClangVC)
[![Version](https://img.shields.io/cocoapods/v/ClangVC.svg?style=flat)](https://cocoapods.org/pods/ClangVC)
[![License](https://img.shields.io/cocoapods/l/ClangVC.svg?style=flat)](https://cocoapods.org/pods/ClangVC)
[![Platform](https://img.shields.io/cocoapods/p/ClangVC.svg?style=flat)](https://cocoapods.org/pods/ClangVC)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ClangVC is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ClangVC'
```

## Author

TYF

## License

ClangVC is available under the MIT license. See the LICENSE file for more info.
