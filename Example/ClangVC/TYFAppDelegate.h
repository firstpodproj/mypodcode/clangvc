//
//  TYFAppDelegate.h
//  ClangVC
//
//  Created by TYF on 01/12/2022.
//  Copyright (c) 2022 TYF. All rights reserved.
//

@import UIKit;

@interface TYFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
