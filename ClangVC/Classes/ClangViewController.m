//
//  ClangViewController.m
//  TestForTXL
//
//  Created by a201111 on 2021/8/26.
//  Copyright © 2021 tyf. All rights reserved.
//

#import "ClangViewController.h"
//clang插桩
#include <stdint.h>
#include <stdio.h>
#include <sanitizer/coverage_interface.h>
#include <dlfcn.h>

@interface ClangViewController ()

@end

@implementation ClangViewController
+ (void)load{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor orangeColor];
}
void __sanitizer_cov_trace_pc_guard_init(uint32_t *start, uint32_t *stop) {
    static uint64_t N;
    if (start == stop || *start) return;
    printf("INIT: %p %p\n", start, stop);
    printf("总计: %x\n", stop-1);
    for (uint32_t *x = start; x < stop; x++)
    *x = ++N;
}
//hook了所有的方法、函数、block
void __sanitizer_cov_trace_pc_guard(uint32_t *guard) {
//    if (!*guard) return;
    void *PC = __builtin_return_address(0);
    Dl_info info;
    dladdr(PC,&info);
    printf("name:%s\n",info.dli_sname);
//  char PcDescr[1024];
//  printf("guard: %p %x PC %s\n", guard, *guard, PcDescr);
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
}

@end
